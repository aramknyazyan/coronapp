Meet SafeRunnie!

A mobile application that will help you be safe while continue training as usual.

Nowadays when it is quarantine we still want to:
- have some walk 
- have some outdoor sports activities 
- have some run.

And stay safe keeping the social distance and for that:
-   we need to know how loaded are the places where we intend to take our sports activities or just walk.

Our Solution:
- SafeRunnie will help you understand the load of a certain place where you intend to take your sports activities.
- SafeRunnie will show you the safety indicator of a certain location advising you on attendance safety.
- SafeRunnie will provide you with a complete picture of safety within a radius of 250 meters.
- SafeRunnie will allow you to keep the history of safe locations and times.